$(function () {
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  var webData;

  $('#calendar-holder').fullCalendar({
    allDaySlot: false,
    minTime: "09:00:00",
    maxTime: "22:00:00",
    eventRender: function(event, element) {
      var dataHoje = new Date();

      category = event.title.split(' - ')[0];
      dateEvent = event.start.format('YYYY-MM-DD');

      $.ajax({
        type: 'GET',
        url: 'siteCheckAjax/'+category+'/'+dateEvent
      }).done(
      function(data){
        if (data.status) {
          if (event.start < dataHoje && event.end > dataHoje) {
            element.attr( 'href', data.link );
            element.attr( 'target', '_blank' );
            element.css('background-color', '#0bed00');
            element.find('.fc-event-inner').append('<span class="fc-event-status"><i class="fa fa-rss"></i> ON AIR</span>');

            if (data.video_code) {
              element.find('.fc-event-inner').append('<span title="Video Code Placed" class="fc-event-status data-ok"><i class="fa fa-video-camera"></i></span>');
            } else {
              element.find('.fc-event-inner').append('<span title="Video Code Missing" class="fc-event-status data-missing"><i class="fa fa-video-camera"></i></span>');
            }

            if (data.featured_image) {
              element.find('.fc-event-inner').append('<span title="Featured Image Placed" class="fc-event-status data-ok"><i class="fa fa-picture-o"></i></span>');
            } else {
              element.find('.fc-event-inner').append('<span title="Featured Image Missing" class="fc-event-status data-missing"><i class="fa fa-picture-o"></i></span>');
            }

          } else if (event.start < dataHoje && event.end < dataHoje) {
            element.attr( 'href', data.link );
            element.attr( 'target', '_blank' );
            element.css('background-color', '#b2b2b2');
            element.find('.fc-event-inner').append('<span class="fc-event-status"><i class="fa fa-check-circle-o"></i> Published</span>');

            if (data.video_code) {
              element.find('.fc-event-inner').append('<span title="Video Code Placed" class="fc-event-status data-ok"><i class="fa fa-video-camera"></i></span>');
            } else {
              element.find('.fc-event-inner').append('<span title="Video Code Missing" class="fc-event-status data-missing"><i class="fa fa-video-camera"></i></span>');
            }

            if (data.featured_image) {
              element.find('.fc-event-inner').append('<span title="Featured Image Placed" class="fc-event-status data-ok"><i class="fa fa-picture-o"></i></span>');
            } else {
              element.find('.fc-event-inner').append('<span title="Featured Image Missing" class="fc-event-status data-missing"><i class="fa fa-picture-o"></i></span>');
            }

          } else if (event.start > dataHoje && event.end > dataHoje) {
            element.attr( 'href', data.link );
            element.attr( 'target', '_blank' );
            element.css('background-color', '#00ceed');
            element.find('.fc-event-inner').append('<span class="fc-event-status"><i class="fa fa-clock-o"></i> Scheduled</span>');

            if (data.video_code) {
              element.find('.fc-event-inner').append('<span title="Video Code Placed" class="fc-event-status data-ok"><i class="fa fa-video-camera"></i></span>');
            } else {
              element.find('.fc-event-inner').append('<span title="Video Code Missing" class="fc-event-status data-missing"><i class="fa fa-video-camera"></i></span>');
            }

            if (data.featured_image) {
              element.find('.fc-event-inner').append('<span title="Featured Image Placed" class="fc-event-status data-ok"><i class="fa fa-picture-o"></i></span>');
            } else {
              element.find('.fc-event-inner').append('<span title="Featured Image Missing" class="fc-event-status data-missing"><i class="fa fa-picture-o"></i></span>');
            }
          }
        } else {
          element.attr( 'href', '#' );
          element.css({'background-color':'#ed0000', 'cursor':'default'});
          element.find('.fc-event-inner').append('<span class="fc-event-status"><i class="fa fa-times-circle-o"></i> Not Scheduled</span>');
        };
      });
},
eventAfterAllRender: function () {
  webData = '(AJAX CALL TO WEBSITE POSTS I THINK SHOULD GO HERE)';
},
eventColor: '#000',
complete: function() {

},
defaultView: 'agendaDay',
googleCalendarApiKey: 'AIzaSyCtEQZsFtsY41kJ1Av5FftgX9u0hTH83WY',
events: {
  googleCalendarId: 'grantcardone.com_l84tadr5fulc7j0628g3g6oj3k@group.calendar.google.com'
},
header: {
  left: 'prev, next,',
  center: 'title',
  right: 'today'
},
titleFormat: '[<span class="showtime-title">#Showtimes</span><span class="showtime-date">]MMMM DD YYYY[</span>]',
lazyFetching: true,
axisFormat: 'h(:mm)a',
timeFormat: {
  agenda: 'h:mmt',
  '': 'h:mmt'
},
weekNumbers: false,
lang: 'en',
eventSources: [
{
  url: Routing.generate('fullcalendar_loader'),
  type: 'POST',
  data: {
  },
  error: function() {
  }
}
]
});


});

var refreshRate;

function reloadTime() {
  refreshRate = setTimeout(reloadPage, 300000); //Refresh Every 5 minutes
}

function reloadPage() {
  $("#calendar-holder").fullCalendar("refetchEvents");
  reloadTime();
}

$( document ).ready(function() {
  reloadTime();
});