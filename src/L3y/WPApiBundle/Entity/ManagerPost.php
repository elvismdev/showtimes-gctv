<?php
/**
 * Created by PhpStorm.
 * User: l3yr0y
 * Date: 6/8/15
 * Time: 9:42 PM
 */

namespace L3y\WPApiBundle\Entity;


use L3y\WPApiBundle\Controller\WPHelperController;

class ManagerPost extends Manager
{
    /**
     * Helper controller
     *
     * @var WPHelperController
     */
    private $helper;

    /**
     * @var array Filter parameters
     */
    private $filter;

    public function __construct()
    {
        $this->helper = new WPHelperController();
        list($this->api_domain, $this->api_base, $this->basic_auth_user, $this->basic_auth_pass) = $this->helper->config();
    }

    /**
     * @param array $parameters
     * @return bool|array
     */
    public function getPosts($parameters = array())
    {
        $this->setFilters($parameters);

        $response = $this->httpGet('posts', $this->filter);

        if ($this->isError())
            return false;

        return (!isset($response->code)) ? $response : false;
    }

    /**
     * @param \DateTime $date
     * @return mixed
     */
    public function findByDate(\DateTime $date)
    {
        $response = $this->getPosts(array('year' => $date->format('Y'), 'monthnum' => $date->format('m'), 'day' => $date->format('d')));

        if ($this->isError())
            return false;

        return (!isset($response->code)) ? $response : false;
    }

    /**
     * @param $category
     * @return mixed
     */
    public function findByCategory($category)
    {
        $response = $this->getPosts(array('category_name' => $this->helper->sanitize($category)));

        if ($this->isError())
            return false;

        return (!isset($response->code)) ? $response : false;
    }

    /**
     * @param array $parameters
     * @return string Well formed URL filter parameters or empty string
     */
    private function setFilters($parameters = array())
    {
        $params = array(
            'm', 'p', 'posts', 'w', 'cat', 'withcomments', 'withoutcomments', 's', 'search', 'exact', 'sentence', 'calendar', 'page',
            'paged', 'more', 'tb', 'pb', 'author', 'order', 'orderby', 'year', 'monthnum', 'day', 'hour', 'minute', 'second', 'name',
            'category_name', 'tag', 'feed', 'author_name', 'static', 'pagename', 'page_id', 'error', 'comments_popup', 'attachment',
            'attachment_id', 'subpost', 'subpost_id', 'preview', 'robots', 'taxonomy', 'term', 'cpage', 'post_type', 'posts_per_page',
            'post_status'
            );

        // Cleaning invalid filter params
        if ($parameters) {
            $parameters = array('filter' => $parameters);
            foreach ($parameters['filter'] as $key => $value) {
                if (!in_array($key, $params))
                    unset($parameters['filter'][$key]);

                if ($key == 'category_name')
                    $parameters['filter'][$key] = $this->helper->sanitize($value);
            }

            $parameters = str_replace(array('%5B', '%5D', '%2C'), array('[', ']', ','), http_build_query($parameters, null, '&'));
        } else {
            $parameters = '';
        }

        $this->filter = $parameters;
    }
}